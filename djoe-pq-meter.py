# python 3.6

import random
import time
import minimalmodbus
import sys
import pprint
import config

from paho.mqtt import client as mqtt_client

broker = '172.16.19.2'
port = 1883
topic = "/kwhMeter/out/"
# generate client ID with pub prefix randomly
client_id = f'python-mqtt-{random.randint(0, 1000)}'


# username = 'emqx'
# password = 'public'

def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
    # client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


def publish(client):
    try:
        rs485 = minimalmodbus.Instrument(config.serial_port, 1)
        rs485.serial.baudrate = 9600
        rs485.serial.bytesize = 8
        rs485.serial.parity = minimalmodbus.serial.PARITY_EVEN
        rs485.serial.stopbits = 1
        rs485.serial.timeout = 1
        rs485.debug = False
        rs485.mode = minimalmodbus.MODE_RTU
        # rs485.precalculate_read_size=True
        rs485.close_port_after_each_call = False

        if len(sys.argv) > 1:
            rs485.address = int(sys.argv[1])
            cmd = sys.argv[2]
            if len(sys.argv) == 4:
                par = eval(sys.argv[3])
            else:
                par = None

            if cmd == "pages":
                print(bin(rs485.read_register(0x112)))
                if par != None:
                    rs485.write_register(0x112, par)

            # number of decimals (0, 1, 2)
            # scrolling time (seconds)
            if cmd == "display":
                print(bin(rs485.read_register(0x113)))
                if par != None:
                    rs485.write_register(0x113, par)

            if cmd == "display2":
                print(bin(rs485.read_register(0x114)))
                if par != None:
                    rs485.write_register(0x114, par)

            # ??? default was 0b1111101000
            if cmd == "test1":
                print(bin(rs485.read_register(0x118)))
                if par != None:
                    rs485.write_register(0x118, par)

            sys.exit(0)

        while True:
            start_time = time.time()

            for id in config.YTL5300_ids:
                rs485.address = id

                influx_measurement = {
                    # "Frequency": rs485.read_float(0x0014),
                    "V1": rs485.read_float(0x000E),
                    "V2": rs485.read_float(0x0010),
                    "V3": rs485.read_float(0x0012),
                    # "I1": float(rs485.read_long(0x139))/1000,
                    "P1": rs485.read_float(0x001E) * 1000,  # active
                    "P2": rs485.read_float(0x0020) * 1000,  # active
                    "P3": rs485.read_float(0x0022) * 1000,  # active
                    "AP1": rs485.read_float(0x0016),
                    "AP2": rs485.read_float(0x0018),
                    "AP3": rs485.read_float(0x001A),
                    "TA": rs485.read_float(0x0100),  # active energy
                    "TP": rs485.read_float(0x001C)
                }

                result = client.publish(topic + str(id), str(influx_measurement))
                # result: [0, 1]
                status = result[0]
                if status == 0:
                    print(f"Sent message to topic `{topic + str(id)}`")
                else:
                    print(f"Failed to send message to topic {topic + str(id)}")

            time.sleep(10)

    except Exception as e:
        print(str(e))
        print("Pausing and restarting...")
        time.sleep(10)


def run():
    client = connect_mqtt()
    client.loop_start()
    publish(client)


if __name__ == '__main__':
    run()
